package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Model.User;
import base.DBManager;

public class UserDAO {

  // Login
  public User findByLoginInfo(String email, String password) {
    Connection con = null;
    try {
      con = DBManager.getConnection();
      String sql =
          "SELECT * FROM user WHERE email = ? and password = ?";

      PreparedStatement pStmt = con.prepareStatement(sql);
      pStmt.setString(1, email);
      pStmt.setString(2, password);

      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      } else {
        String name = rs.getString("name");
        int Id = rs.getInt("id");
        return new User(name, Id);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }
}
