package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import Model.Todo;
import base.DBManager;

public class TodoDAO {

  //
  public static List<Todo> findAll(int userId) throws SQLException {
    Connection con = null;
    List<Todo> todoList = new ArrayList<Todo>();

    try {
      con = DBManager.getConnection();

      String sql =
          "SELECT * FROM todo WHERE user_id = ? ORDER BY todo.id DESC";

      PreparedStatement ps = con.prepareStatement(sql);
      ps.setInt(1, userId);

      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        Todo todo = new Todo();
        todo.setId(rs.getInt("id"));
        todo.setTitle(rs.getString("title"));
        todo.setUser_id(rs.getInt("user_id"));

        todoList.add(todo);
      }
      System.out.println("getAllTODO completed");
      return todoList;
    } catch (SQLException e) {
      System.out.println(e.getMessage());
      throw new SQLException(e);
    } finally {
      if (con != null) {
        con.close();
      }
    }
  }


  // 新規追加
  public void addTodo(String title, int userId) {
    Connection con = null;

    try {
      con = DBManager.getConnection();

      String sql = "INSERT INTO todo(title,user_id) VALUES(?,?)";

      PreparedStatement ps = con.prepareStatement(sql);
      ps.setString(1, title);
      ps.setInt(2, userId);

      ps.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  // Update
  public void update(int id, String title) {
    Connection con = null;

    try {
      con = DBManager.getConnection();
      String sql = "UPDATE todo SET title = ? WHERE id = ?";

      PreparedStatement ps = con.prepareStatement(sql);
      ps.setString(1, title);
      ps.setInt(2, id);


      ps.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
      return;
    } finally {
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return;
        }
      }
    }
    return;
  }

  public List<Todo> search(String title) {
    Connection con = null;
    List<Todo> todolist = new ArrayList<Todo>();

    try {
      con = DBManager.getConnection();

      String sql = "SELECT * FROM todo  ";
      StringBuilder str = new StringBuilder(sql);
      List<String> list = new ArrayList<String>();

      // if (title.equals("")) {
      // str.append("");
      // list.add(title);
      // }
      if (!(title.equals(""))) {
        str.append(" WHERE title Like ?");
        list.add("%" + title + "%");
      }
      PreparedStatement ps = con.prepareStatement(str.toString());

      for (int i = 0; i < list.size(); i++) {
        ps.setString(i + 1, list.get(i));

      }
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        Todo todo = new Todo();
        todo.setId(rs.getInt("id"));
        todo.setTitle(rs.getString("title"));
        todo.setUser_id(rs.getInt("user_id"));

        todolist.add(todo);
      }
      // return todolist;
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return todolist;
  }

  //
  // public Todo delete(String title, int id) {
  public Todo delete(int id) {
    Connection con = null;

    try {
      con = DBManager.getConnection();
      String sql = "DELETE FROM todo WHERE id = ?";

      PreparedStatement ps = con.prepareStatement(sql);
      ps.setInt(1, id);
      ps.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return null;
  }


  public Todo findByid(int id) {
    Connection con = null;
    Todo todo = null;
    PreparedStatement ps = null;

    try {
      con = DBManager.getConnection();

      String sql = "SELECT * FROM todo WHERE id = ?";
      ps = con.prepareStatement(sql);

      ps.setInt(1, id);
      ResultSet rs = ps.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int Id = rs.getInt("id");
      String title = rs.getString("title");
      int userId = rs.getInt("user_id");

      todo = new Todo(Id, title, userId);
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (con != null) {
        try {
          con.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return todo;
  }
}
