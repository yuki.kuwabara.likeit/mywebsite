package Controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import DAO.TodoDAO;
import Model.User;

/**
 * Servlet implementation class Create
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public CreateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
    dispatcher.forward(request, response);
    return;

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    TodoDAO tododao = new TodoDAO();
    String title = request.getParameter("title");

    if (title.equals("")) {
      request.setAttribute("errMsg", "NOT FILL IN TITLE");

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
      dispatcher.forward(request, response);
      return;

    }
    tododao.addTodo(title, user.getId());

    response.sendRedirect("ListServlet");

  }
}
