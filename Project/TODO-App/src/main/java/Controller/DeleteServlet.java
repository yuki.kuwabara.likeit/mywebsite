package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import DAO.TodoDAO;
import Model.Todo;
import Model.User;

/**
 * Servlet implementation class Delete
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public DeleteServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) //
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
    int id = Integer.valueOf(request.getParameter("id"));

    TodoDAO todoDao = new TodoDAO();
    todoDao.delete(id);

    // request.getRequestDispatcher("/WEB-INF/jsp/list.jsp").forward(request, response);;
    response.sendRedirect("ListServlet");
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");

    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    try {
      List<Todo> todoList = TodoDAO.findAll(user.getId());

      if (user != null) {
        request.setAttribute("todoList", todoList);

        request.getRequestDispatcher("/WEB-INF/jsp/list.jsp").forward(request, response);
      } 
        request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
        return;

    } catch (SQLException e) {
      // TODO 自動生成された catch ブロック
      e.printStackTrace();
    }
  }
}
