package util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.xml.bind.DatatypeConverter;

public class PasswordEncorder {

  public static String encordPassword(String password) {

    Charset charset = StandardCharsets.UTF_8;
    String algorithm = "MD5";

    try {
      byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
      String encodepass = DatatypeConverter.printHexBinary(bytes);

      System.out.println(encodepass);
      return encodepass;
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return algorithm;
  }
}
