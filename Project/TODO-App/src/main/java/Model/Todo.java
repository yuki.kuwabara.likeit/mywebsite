package Model;

import java.io.Serializable;

public class Todo implements Serializable {

  private int id;
  private String title;
  private int user_id;

  public Todo() {
    // TODO 自動生成されたコンストラクター・スタブ
  }
  public Todo(int Id, String title, int userId) {
    this.id = Id;
    this.title = title;
    this.user_id = userId;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getUser_id() {
    return user_id;
  }

  public void setUser_id(int user_id) {
    this.user_id = user_id;
  }



}
